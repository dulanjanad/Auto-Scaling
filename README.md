# Auto-Scaling
## AWS Auto Scaling Group with Terrafrom

#### Prerequisites

1). Terraform CLI installed in your machine.

2). Git installed in your machine.

3). An AWS Account with an IAM User with permissions to create,modify,list resources.

4). Configured AWS CLI with use of above IAM user.

#### Bit of changes I have done to the architecture

1). VPC endpoint and S3 Access endpoint would be used to communicate with private S3 Bucket (Diagram-1)

![Diagram-1](Diagrams/Auto-Scaling.png)

2). IAM Policies and Roles to interact with AWS resources.

3). Used a SSH key to login to the servers in case of troubleshooting events.

4). Allowed port 22 access to EC2 fleet only for privaate CIDR block "172.31.0.0/16" (singapre region's default VPC CIDR block).

5). Used "172.31.64.0/20" for private subnet.


### Deploy AWS Resources

1). Clone GIT repository.

    https://gitlab.com/dulanjanad/Auto-Scaling.git
    
2). Initialize Terraform.

    terraform init

3). Validate Terraform code.

    terraform validate
    
4). Run Terraform plan 

    terraform plan
    
5). Apply the changes.

    terraform apply -auto-approve

6). Once everything deployed successfully you can grab the DNS record for ALB from outputs and check it from a browser.

![Diagram-2](Diagrams/Before-Update.png)


### GitLab pipeline

A GitLab pipeline has been created on (https://gitlab.com/dulanjanad/Incremental-Decremental-Counter) repository to push updates into S3 bucket and refresh the auto-scaling group automcatically

When updates from a branch to master branch pipeline will run automcatically.

Pipleline take care of below two steps

1). Push updates to index.html file to S3 bucket

    aws s3 cp index.html s3://<bucket_name>/index.html
    
2). Refresh auto-scaling group

    aws autoscaling start-instance-refresh --auto-scaling-group-name <auto-scaling-group name goes here...>
    
    If it successful should get an JSON output like below in your terminal

    {
        "InstanceRefreshId": "a23c5b94-3fe5-4c10-a0a7-d40bb0c7994b"
    }

Reload the same URL copied from ALB earlier in few mins, you will see the updated content on web-page

![Diagram-3](Diagrams/After-Update.png)



### Improvements for the architecture and setup

1). Deploy resources in a privaate VPC rather than default. This will have more control over subnets

2). Make sure EC2 auto-scaling fleet is deployed in more than one availability zone - To acheieve High Availability.

3). Setup AWS WAF infront of ALB - For better security.

4). Use a Golden AMI bundled with Nginx for deployments - Reduce the time for deployments.

5). Use Dedicated AWS EC2 instances rather than on demand - Overcome resource limitations from AWS

6). Use a proper deployment strategy. - ex: Blue Green 
