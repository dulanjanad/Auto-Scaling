region = "ap-southeast-2"

private_subnet_cidr = "172.31.64.0/20"

availability_zone = "ap-southeast-2a"

availability_zone_2 = "ap-southeast-2b"

bucket_name = "yay-private-bucket-50000000"